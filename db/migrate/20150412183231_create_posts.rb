class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :link, uniq: true
      t.boolean :posted, default: false
      t.datetime :posted_at

      t.timestamps null: false
    end
  end
end
