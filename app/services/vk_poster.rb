require 'open-uri'
require 'faraday'

module VkPoster
  module_function

  @vk_app = VkontakteApi::Client.new(Rails.application.secrets.vk_token)

  def post_to_wall(link)
    file = file_path(link)
    save_file(link, file)
    upload_to_wall(file)
    File.delete(file)
  end

  def upload_to_album(link, title)
    file = file_path(link)
    save_file(link, file)
    if %(gif).include?(mime_type(file).subtype)
      upload = VkontakteApi.upload(url: docs_server, file: [file, mime_type(file)])
      @vk_app.docs.save(file: upload[:file], tags: 'ffffound')
    else
      album_id = album_exists?(title) ? find_album_by_title(title) : create_album(title)[:aid]
      upload = VkontakteApi.upload(url: photos_album_server(album_id), file1: [file, mime_type(file)])
      @vk_app.photos.save(album_id: album_id,
                          group_id: ENV['VK_GROUP_ID'],
                          server: upload[:server],
                          photos_list: upload[:photos_list],
                          hash: upload[:hash])
    end
    File.delete(file)
  end

  def upload_to_wall(file)
    if %w(jpeg png).include?(mime_type(file).subtype)
      upload = VkontakteApi.upload(url: photos_server, photo: [file, mime_type(file)])
      post_item = @vk_app.photos.save_wall_photo(upload.merge(gid: ENV['VK_GROUP_ID']))
      @vk_app.wall.post(owner_id: "-#{ENV['VK_GROUP_ID']}", attachments: post_item.map(&:id).join(','))
    else
      upload = VkontakteApi.upload(url: docs_server, file: [file, mime_type(file)])
      post_item = @vk_app.docs.save(upload.merge(gid: ENV['VK_GROUP_ID'])).first
      doc = "doc#{post_item[:owner_id]}_#{post_item[:did]}"
      @vk_app.wall.post(owner_id: "-#{ENV['VK_GROUP_ID']}", attachments: doc)
    end
  end

  def create_album(name)
    @vk_app.photos.create_album(title: name, group_id: ENV['VK_GROUP_ID'], upload_by_admins_only: 1)
  end

  def mime_type(file)
    MimeMagic.by_path(file)
  end

  def photos_wall_server
    @vk_app.photos.get_wall_upload_server(group_id: ENV['VK_GROUP_ID'])[:upload_url]
  end

  def docs_server
    @vk_app.docs.get_wall_upload_server(group_id: ENV['VK_GROUP_ID'])[:upload_url]
  end

  def photos_album_server(album_id)
    @vk_app.photos.get_upload_server(album_id: album_id, group_id: ENV['VK_GROUP_ID'])[:upload_url]
  end

  def group_albums
    @vk_app.photos.get_albums(owner_id: "-#{ENV['VK_GROUP_ID']}")
  end

  def find_album_by_title(title)
    group_albums.each do |item|
      return item[:aid] if item[:title] == title
    end
  end

  def album_exists?(title)
    group_albums.map(&:title).include?(title)
  end

  def save_file(link, file_path)
    File.open(file_path, 'wb') { |fo| fo.write open(link).read }
  end

  def file_path(link)
    name = link.split('/').last
    "#{Rails.root}/tmp/#{name}"
  end
end
