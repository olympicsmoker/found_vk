require 'nokogiri'
require 'open-uri'

module FoundParser
  def self.string_time_to_object(times)
    sum = 0
    times.each do |time|
      obj = time.split(' ')
      num = obj[0].to_i
      sum += num.send(obj[1])
    end
    sum
  end

  def self.parse(page)
    parsed_page = []
    dates = (2000..2015).to_a
    string_date = ''
    doc = Nokogiri::HTML(open("#{ENV['FOUND_SITE']}#{page * 25}"))
    doc.css('blockquote.asset').each do |block|
      block.css('.description').map { |node| node.children.map(&:text) }.flatten.each do |item|
        string_date = item.squish if dates.any? { |d| item.include?(d.to_s) }
      end
      if string_date.match('(\s?(\d+)\s(hours|minutes|days))+\s(ago)').present?
        string_date = string_time_to_object(string_date.scan(/(\d+\s\w+)/i).flatten)
        date =  (DateTime.now - string_date.seconds).to_datetime
      else
        date = string_date.to_datetime
      end
      image_src = block.css("a img[@id*='asset']")[0] || ''
      parsed_page << { link: image_src, posted_at: date }
    end
    parsed_page
  end
end
