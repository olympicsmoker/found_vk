class Post < ActiveRecord::Base
  validates_presence_of :link
  validates_presence_of :posted_at

  def mark_posted!
    update_attributes(posted: true)
  end
end
