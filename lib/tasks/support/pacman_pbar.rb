class PacmanPbar < ProgressBar::Base
  def initialize(size = 0, title = 'processing')
    @pbar = ProgressBar.create(title: title,
                               total: size,
                               format: '%a %bᗧ%i %p%% %t',
                               progress_mark:  ' ',
                               remainder_mark: '･',
                               starting_at: 1)
  end

  def increment
    @pbar.increment
  end

  def finish
    @pbar.finish
  end
end
