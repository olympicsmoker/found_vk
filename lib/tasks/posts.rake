namespace :posts do
  desc 'populate posts'
  task :populate, [:start, :stop] do |_t, args|
    ActiveRecord::Base.logger.level = 2
    range = args[:start].to_i..args[:stop].to_i
    pbar = PacmanPbar.new(args[:stop].to_i * 25, 'creating posts')
    range.to_a.reverse_each do |page|
      pbar.increment
      FoundParser.parse(page).each { |post_params| Post.create(post_params) }
    end
    pbar.finish
  end

  desc 'upload to vk album'
  task :upload_to_album do
    ActiveRecord::Base.logger.level = 2
    unposted = Post.where(posted: false).order(posted_at: :asc)
    pbar = PacmanPbar.new(unposted.count, 'creating posts')
    unposted.each_slice(200) do |slice|
      pbar.increment
      slice.each do |post|
        next unless Post.where(link: post.link).present?
        VkPoster.upload_to_album(post.link, post.posted_at.year.to_s)
        post.mark_posted!
      end
      sleep 60
    end
    pbar.finish
  end

  desc 'cleaning db'
  task :clean_db do
    Post.destroy_all
    ActiveRecord::Base.connection.reset_pk_sequence!('posts')
  end
end
