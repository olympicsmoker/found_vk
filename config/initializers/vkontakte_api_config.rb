VkontakteApi.configure do |config|
  config.app_id       = Rails.application.secrets.vk_app_id
  config.app_secret   = Rails.application.secrets.vk_app_secret
  config.redirect_uri = 'https://oauth.vk.com/blank.html'

  config.http_verb = :post
end
